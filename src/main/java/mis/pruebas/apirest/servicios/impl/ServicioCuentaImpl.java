package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    public final Map<String,Cuenta> Cuentas= new ConcurrentHashMap<String,Cuenta>();

    @Override
    public List<Cuenta> obtenerTodosCuentas() {


        if(this.Cuentas.size()==0){

            throw new RuntimeException("Cuenta vacia: ");
        }

        return List.copyOf(this.Cuentas.values());

    }

    @Override
    public void insertarCuentaNuevo(Cuenta Cuenta) {
        this.Cuentas.put(Cuenta.numero,Cuenta);

    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        if (!this.Cuentas.containsKey(numero))
            throw new RuntimeException("No existe el Cuenta: " + numero);
        return this.Cuentas.get(numero);
    }

    @Override
    public void guardarCuenta(Cuenta Cuenta) {
        if (!this.Cuentas.containsKey(Cuenta.numero))
            throw new RuntimeException("No existe el Cuenta: " + Cuenta.numero);

        this.Cuentas.replace(Cuenta.numero,Cuenta);

    }

    @Override
    public void emparcharCuenta(Cuenta parche) {
        final Cuenta existente = this.Cuentas.get(parche.numero);

        if(parche.tipo != existente.tipo)
            existente.tipo = parche.tipo;

        if(parche.saldo != null)
            existente.saldo = parche.saldo;

        if(parche.estado != null)
            existente.estado = parche.estado;

        if(parche.moneda != null)
            existente.moneda = parche.moneda;



        this.Cuentas.replace(existente.numero, existente);
    }


    @Override
    public void borrarCuenta(String numero) {
        if (!this.Cuentas.containsKey(numero))
            throw new RuntimeException("No existe el Cuenta: " + numero);

        this.Cuentas.remove(numero);
    }


}

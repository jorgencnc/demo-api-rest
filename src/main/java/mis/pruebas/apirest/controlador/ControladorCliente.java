package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@RequestMapping(Rutas.CLIENTES)
@CrossOrigin
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;



   @GetMapping
   public ResponseEntity<List<Cliente>> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {
       try {
           return ResponseEntity.ok(this.servicioCliente.obtenerClientes(pagina - 1, cantidad));
       } catch(Exception x) {
           return ResponseEntity.notFound().build();

       }
   }

   @PostMapping
   public ResponseEntity agregarCliente(@RequestBody Cliente cliente) {


        this.servicioCliente.insertarClienteNuevo(cliente);

       final var representacionMetodoObtenerUnClienteConDocumento = methodOn(ControladorCliente.class)
               .obtenerUnCliente(cliente.documento);

       final var enlaceEsteDocumento = linkTo(representacionMetodoObtenerUnClienteConDocumento).toUri();


      return ResponseEntity
                .accepted()
               .location(enlaceEsteDocumento)
               .body("Cliente creado con éxito!");


   }

    @GetMapping("/{documento}")
    public EntityModel<Cliente> obtenerUnCliente(@PathVariable String documento) {
        try {
            final Link self = linkTo(methodOn(this.getClass()).obtenerUnCliente(documento)).withSelfRel();
            final Link cuentas = linkTo(methodOn(ControladorCuentasCliente.class).obtenerCuentasCliente(documento)).withRel("cuentas");
            final Link clientes = linkTo(methodOn(this.getClass()).obtenerClientes(1, 100)).withRel("todosLosClientes");

            final var cliente = this.servicioCliente.obtenerCliente(documento);
            return EntityModel.of(cliente).add(self, cuentas, clientes);
        } catch(Exception x) {

            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

  @PutMapping("/{documento}")
   public ResponseEntity  reemplazarUnCliente(@PathVariable("documento") String nroDocumento,@RequestBody Cliente cliente) {
        try {
            cliente.documento=nroDocumento;
            this.servicioCliente.guardarCliente(cliente);
            return new ResponseEntity<>("Cliente actualizado correctamente.", HttpStatus.OK);
        }catch (Exception x) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }

   }

    @PatchMapping("/{documento}")
    public ResponseEntity emparacharUnCliente(@PathVariable("documento") String nroDocumento,@RequestBody Cliente cliente) {
        try {

            cliente.documento=nroDocumento;
            this.servicioCliente.emparcharCliente(cliente);
            return new ResponseEntity<>("Cliente actualizado correctamente.", HttpStatus.OK);
        }catch (Exception x) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{documento}")
    public ResponseEntity  eliminarUnCliente(@PathVariable String documento) {
        try {
            this.servicioCliente.borrarCliente(documento);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch(Exception x) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
    }





}

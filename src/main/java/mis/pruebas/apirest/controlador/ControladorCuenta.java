package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.CUENTAS)
@CrossOrigin
public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    @GetMapping
    public ResponseEntity<List<Cuenta>> obtenerCuenta() {
        try {
            return ResponseEntity.ok( this.servicioCuenta.obtenerTodosCuentas());
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity agregarCuenta(@RequestBody Cuenta Cuenta) {


        this.servicioCuenta.insertarCuentaNuevo(Cuenta);

        final var representacionMetodoObtenerUnClienteConDocumento = methodOn(ControladorCuenta.class)
                .obtenerUnCuenta(Cuenta.numero);

        final var enlaceEsteDocumento = linkTo(representacionMetodoObtenerUnClienteConDocumento).toUri();


        return ResponseEntity
                .accepted()
                .location(enlaceEsteDocumento)
                .body("Cuenta creado con éxito!");


    }


    @GetMapping("/{numero}")
    public  EntityModel<Cuenta> obtenerUnCuenta(@PathVariable String numero) {



        try {
            final Link self = linkTo(methodOn(this.getClass()).obtenerUnCuenta(numero)).withSelfRel();


            final var cuenta = this.servicioCuenta.obtenerCuenta(numero);
            return EntityModel.of(cuenta).add(self);
        } catch(Exception x) {

            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{numero}")
    public ResponseEntity  reemplazarUnCuenta(@PathVariable("numero") String nroNumero,@RequestBody Cuenta Cuenta) {
        try {
            Cuenta.numero=nroNumero;
            this.servicioCuenta.guardarCuenta(Cuenta);
            return new ResponseEntity<>("Cuenta actualizado correctamente.", HttpStatus.OK);
        }catch (Exception x) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }

    }



    @PatchMapping("/{numero}")
    public ResponseEntity emparacharUnCuenta(@PathVariable("numero") String nroNumero,@RequestBody Cuenta Cuenta) {
        try {
        Cuenta.numero=nroNumero;
        this.servicioCuenta.emparcharCuenta(Cuenta);
        return new ResponseEntity<>("Cuenta actualizado correctamente.", HttpStatus.OK);
        }catch (Exception x) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{numero}")
    public ResponseEntity eliminarCuenta(@PathVariable("numero") String numero) {

        try {
            this.servicioCuenta.borrarCuenta(numero);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch(Exception x) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }

    }





}
